import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import Home from './src/screen/Home';
import BukuPuisi from './src/screen/BukuPuisi';
import Akun from './src/screen/Akun';
import Login from './src/screen/Login';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function App() {
  return (
    <NavigationContainer>
      {/* <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
        <Stack.Screen name="BukuPuisi" component={BukuPuisi} options={{headerShown: false}} />
        <Stack.Screen name="Akun" component={Akun} options={{headerShown: false}} />
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
      </Stack.Navigator> */}

      <Tab.Navigator
      initialRouteName='Home'
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, size, color}) => {
          let iconName;
          if (route.name == 'Home'){
            iconName = focused ? 'home' : 'home';
          } else if (route.name == 'Kumpulan Puisi'){
            iconName = focused ? 'book' : 'book';
          } else if (route.name == 'Akun'){
            iconName = focused ? 'user-circle-o' : 'user-circle-o';
          } 
           return <Icon name={iconName} size={size} colour={color}/>
        },
      })}> 
        <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
        <Tab.Screen name="Kumpulan Puisi" component={BukuPuisi} options={{headerShown: false}}  />
        <Tab.Screen name="Akun" component={Akun} options={{headerShown: false}} />

      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default App;

