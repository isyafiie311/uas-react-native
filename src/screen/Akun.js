import React, { useState, useEffect } from 'react';
import { View, Text, StatusBar, Image, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Akun = ({navigation}) => {
  return (
    <View style={{flex: 1}}><StatusBar barStyle={'light-content'}  backgroundColor='#3880ff'/>
    <View style={{flex: 1, backgroundColor: '#3880ff', borderBottomLeftRadius: 30, borderBottomRightRadius: 30}}>
    <View style={{flex: 1, alignItems: 'center', marginTop: 120 }}><Text style={{fontWeight: 'bold', fontSize: 50, color: '#FFFFFF'}}>PuisiBook.</Text></View>
    </View>
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
    <View style={{justifyContent: 'center',alignItems: 'center'}}>
      <Image source={require('../../assets/images/ayam.jpg')}
      style={{width: 100, height: 100, borderRadius: 100 / 2, position: 'absolute'}}/>
      </View>
      <View>
        <Text style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center', marginTop: 60}}>Imam Syafi'ie</Text>
        <Text style={{textAlign: 'center',}}>Akun saat ini.</Text>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')} style={{backgroundColor: '#3880ff', paddingVertical: 10, marginTop: 20, marginHorizontal: 10, borderRadius: 15,}}>
            <Text style={{color: '#FFFFFF', textAlign: 'center', fontSize: 20}}>Login</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity onPress={() => Alert.alert('Penting','Tekan Tombol Kembali Anda.')} style={{backgroundColor: '#3880ff', paddingVertical: 10, marginTop: 20, marginHorizontal: 10, borderRadius: 15,}}>
            <Text style={{color: '#FFFFFF', textAlign: 'center', fontSize: 20}}>Log-Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
    </View>
  );
};

export default Akun;

// import React, { Component, useEffect, useState } from 'react'
// import { View, Text, StatusBar } from 'react-native';
// class Akun extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {  };
//     }
//     render() {
//         return (
//             <View style={{flex: 1}}>
//             <StatusBar barStyle={'light-content'} backgroundColor='#3880ff'/> 
//                 <View style={{flex: 0,7 backgroundColor: '#3880ff'}}></View>
//             </View>
//         );
//     }
// }

// export default Akun;