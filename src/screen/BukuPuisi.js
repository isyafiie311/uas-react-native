import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
  StatusBar, Alert, StyleSheet
} from 'react-native';

import { ScrollViewComponent, SliderComponent } from "react-native";


const BukuPuisi= ({navigation}) => {



  const [daftarRekomendasi, setDaftarRekomendasi] = useState([
    {
      judul: 'Laskar pelangi',
      image: require('../../assets/images/puisi1.jpg'),
      deskripsi: 'Garin Nugroho',
    },
    {
      judul: 'GLOSARI SA PAGGAWA NG DAMIT',
      image: require('../../assets/images/puisi2.jpg'),
      deskripsi: 'KRISTYN T. CARAGAY',
    },
    {
      judul: 'ANG TAO MAPULAAN LABAW NA UG PIRMI MASAKATI.',
      image: require('../../assets/images/puisi3.jpg'),
      deskripsi: 'Community Puisi',
    },
    {
      judul: 'PARSA',
      image: require('../../assets/images/puisi4.jpg'),
      deskripsi: 'Community Puisi',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#3880ff',}}>
      <StatusBar backgroundColor="#3880ff" barStyle="light-content" />
      <View style={{marginHorizontal: 20, alignItems: 'center', marginTop: 20}}>
              <Text style={{fontSize: 30, fontWeight: 'bold', color: '#FFFFFF'}}>
              Kumpulan Puisi
            </Text>
            <Text style={{fontSize: 25, fontWeight: 'bold', color: '#FFFFFF'}}>
              PuisiBook.
            </Text>
          </View>
      <View style={{flex: 1}}>
        <View>
          <View style={{marginLeft: 50, marginTop: 10}}>
            <FlatList
              data={daftarRekomendasi}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => Alert.alert('Penting','Maaf ini hanya sampul puisi belum tersedia.')}
                  style={{
                    backgroundColor: '#FFFFFF',
                    alignItems: 'center',

                    marginTop: 10,
                    flexDirection: 'column-reverse',
                    marginRight: 50,
                    elevation: 3,
                    padding: 10,
                    marginBottom: 10,
                    borderRadius: 5,
                  }}>
                  <View style={{margincolumn: 10, width: 200, alignItems: 'center',}}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#212121',
                      }}>
                      {item.judul}
                    </Text>
                    <Text style={{fontSize: 14, alignItems: 'center',}}>{item.deskripsi}</Text>
                  </View>
                  <View>
                    <Image
                      source={item.image}
                      style={{width: 130, height: 150, borderRadius: 5 }}
                      resizeMode="contain"
                    />
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </View>

    </View>
  );
};

export default BukuPuisi;