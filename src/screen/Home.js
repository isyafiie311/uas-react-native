import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ScrollView,
  StatusBar, Alert, StyleSheet
} from 'react-native';

import { ScrollViewComponent, SliderComponent } from "react-native";


const Home = ({navigation}) => {
  const [dataBuku, setDataBuku] = useState([
    {
      judul: 'Laskar pelangi',
      image: require('../../assets/images/puisi1.jpg'),
      author: 'Garin Nugroho',
    },
    {
      judul: 'GLOSARI SA PAGGAWA NG DAMIT',
      image: require('../../assets/images/puisi2.jpg'),
      author: 'KRISTYN T. CARAGAY',
    },
    {
      judul: 'ANG TAO MAPULAAN LABAW NA UG PIRMI MASAKATI.',
      image: require('../../assets/images/puisi3.jpg'),
      author: 'Community Puisi',
    },
    {
      judul: 'PARSA',
      image: require('../../assets/images/puisi4.jpg'),
      author: 'Community Puisi',
    },
    {
      judul: 'Surat Kecil Untuk TUHAN',
      image: require('../../assets/images/puisi5.jpeg'),
      author: 'ANDI F.NOYA, HOST KICK ANDY',
    },
  ]);

  const [daftarRekomendasi, setDaftarRekomendasi] = useState([
    {
      judul: 'GLOSARI SA PAGGAWA NG DAMIT',
      deskripsi: 'KRISTYN T. CARAGAY',
      image: require('../../assets/images/puisi2.jpg'),
    },
    {
      judul: 'ANG TAO MAPULAAN LABAW NA UG PIRMI MASAKATI.',
      deskripsi: 'Community Puisi',
      image: require('../../assets/images/puisi3.jpg'),
    },
    {
      judul: 'PARSA',
      deskripsi: 'Community Puisi',
      image: require('../../assets/images/puisi4.jpg'),
    },
  ]);

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="#3880ff" barStyle="light-content" />
      <ScrollView style={{flex: 1}}>
        <View
          style={{
            backgroundColor: '#3880ff',
            borderBottomLeftRadius: 30,
            paddingBottom: 10,
            elevation: 5,
          }}>
          <View style={{marginHorizontal: 20, marginTop: 10}}>
            <Text style={{color: '#FFFFFF'}}>Selamat datang Dan Selamat membaca di </Text>
            <Text style={{fontSize: 30, fontWeight: 'bold', color: '#FFFFFF'}}>
              PuisiBook.
            </Text>
          </View>

          <View style={{marginLeft: 20, marginTop: 20}}>
            <FlatList
              data={daftarRekomendasi}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => Alert.alert('Penting','Maaf ini hanya sampul puisi belum tersedia.')}
                  style={{
                    backgroundColor: '#FFFFFF',

                    marginTop: 10,
                    flexDirection: 'row',
                    marginRight: 20,
                    elevation: 3,
                    padding: 10,
                    marginBottom: 10,
                    borderRadius: 5,
                  }}>
                  <View style={{marginRight: 10, width: 200}}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 20,
                        color: '#212121',
                      }}>
                      {item.judul}
                    </Text>
                    <Text style={{fontSize: 14}}>{item.deskripsi}</Text>
                  </View>
                  <View>
                    <Image
                      source={item.image}
                      style={{width: 130, height: 150, borderRadius: 5}}
                      resizeMode="contain"
                    />
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>

        <View style={{marginLeft: 20, marginTop: 20}}>
          <View style={{flexDirection: 'row', marginRight: 10}}>
            <Text style={{fontWeight: 'bold', color: '#212121'}}>
              Puisi UKM-PI
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Kumpulan Puisi')}
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <Text>Lihat Semua</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={dataBuku}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => Alert.alert('Penting','Maaf ini hanya sampul puisi belum tersedia.')}
                style={{
                  width: 150,
                  backgroundColor: '#FFFFFF',

                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  marginTop: 10,
                }}>
                <Image
                  source={item.image}
                  style={{width: 130, height: 150, borderRadius: 5}}
                  resizeMode="contain"
                />
                <Text style={{fontWeight: 'bold'}}>{item.judul}</Text>
                <Text style={{fontSize: 14}}>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View style={{marginLeft: 20, marginTop: 20, marginBottom: 20}}>
          <View style={{flexDirection: 'row', marginRight: 10}}>
            <Text style={{fontWeight: 'bold', color: '#212121'}}>Kumpulan Puisi</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Kumpulan Puisi')}
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <Text>Lihat Semua</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={dataBuku}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => Alert.alert('Penting','Maaf ini hanya sampul puisi belum tersedia.')}
                style={{
                  width: 150,
                  backgroundColor: '#FFFFFF',

                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  marginTop: 10,
                }}>
                <Image
                  source={item.image}
                  style={{width: 130, height: 150, borderRadius: 5}}
                  resizeMode="contain"
                />
                <Text style={{fontWeight: 'bold'}}>{item.judul}</Text>
                <Text style={{fontSize: 14}}>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>

{/* tempat buttom navigation */}

    </View>
  );
};

export default Home;